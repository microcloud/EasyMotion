#ifndef ROBOTMODEL_H
#define ROBOTMODEL_H
#include <math.h>
#include <eigen3/Eigen/Dense>
void copyData(int * dst,int*src,int len,float k=1.0);
void copyData(int ** dst,int** src,int len,int hight,float k=1.0);
void copyDataBlock(int * dst,int* src,int len,int hight,float k=1.0);

using  Eigen::VectorXi;
using  Eigen::Vector3f;
/*
将难理解的刻度转换为 向量来表示。


*/
struct angle_info
{
    float left_leg  [4];//rotateX, rotateY, rotateZ, len
    float right_leg [4];
    float lef_hand  [3];//rotateX, rotateY, len
    float right_hand[3];
    float left_foot [2];//rotateX, rotateY
    float right_foot[2];
};
enum side
{
    left=1,
    right
};
 class robotModel
{
    public:
        angle_info info;
        robotModel();
        void setLeg(double rotateX,double rotateY,double rotateZ,double len,int leg);
        void setFoot(double rotateX,double rotateY,int leg);
        void getData(int *d);//写出舵机数据
        void modifyData(int *d);//将外部数据写入。

        void showParam(int leg);//
        void updataParam(int leg);
        virtual ~robotModel();
        Vector3f left_legAngle;
        Vector3f right_legAngle;

        Vector3f left_info;
        Vector3f right_info;

        VectorXi m_array;
    protected:
    private:

        const float d_p_s=300.0/1024;
        const float r_p_s=M_PI/614.4;
        const float leg_length=11;
        int *m_data;
};

#endif // ROBOTMODEL_H
