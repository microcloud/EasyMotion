#ifndef GAITSTEP_H
#define GAITSTEP_H
#include <string>
#include <eigen3/Eigen/Dense>

#include "robotModel.h"
using std::string;
using Eigen::ArrayXXi;

typedef int motorArray[20];
typedef Eigen::Array<int ,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> matrix;
/*
记录步态信息
创建len*20的矩阵来记录每一步。

有待改进：
文件读取、写入
步态之间的操作：如累加、补充。
数据存放
*/

class gaitStep
{
    public:
        int step=0;
        gaitStep(string ,int );
        bool loadFile(string);
        bool saveFile();
        bool pushData(int* d);
        bool popData(int *d);
        int getLen(){return length;}
        void getData(int *);
        string name;
        virtual ~gaitStep();
        matrix m_gait;
    protected:
    private:

        int length;
        int* m_data;
        int used[20];//unused
};

#endif // GAITSTEP_H
