#ifndef PROTOCOL_H
#define PROTOCOL_H
#include "Serial.h"

struct T_MOTION_PACK
{
    char head[2];
    char type;
    char none;
    char data[40];
    char checksume;
};
struct R_MOTION_PACK
{
    char head[2];

};
class Protocol
{
    public:
        Serial *dev;
        char * tmp_recv;
        Protocol(Serial *);
        bool recieve();
        virtual ~Protocol();
    protected:
    private:
        int RTIME;
};

#endif // PROTOCOL_H
