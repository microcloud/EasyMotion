#ifndef SERIAL_H
#define SERIAL_H

#include <string>
using namespace std;
class Serial
{
    public:
        Serial(char *dev, int baudRate=57600);
        bool set_speed(int );
        bool set_Parity(int databits,int stopbits,int parity);
        virtual ~Serial();
        bool ReadSerial(char * ,int);
        int available();
        bool WriteSerial(char *,int );
    protected:
    private:
        int fd;
};

#endif // SERIAL_H
