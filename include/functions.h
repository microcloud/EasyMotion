#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <eigen3/Eigen/Dense>
#include "gaitStep.h"
/*
提供简便有效的方法来处理步态生成步态信息。

robotmodel可创建某一时刻的状态。

*/
gaitStep generateGait_line(motorArray a,motorArray b,int length);
gaitStep generateGait_rate(motorArray a,motorArray b,int length);
using namespace Eigen;



#endif // FUNCTIONS_H
