#ifndef ACTION_H
#define ACTION_H

#include <vector>
typedef void (*ACTION_FUN)(int data[20],double s);

//a1= a1+a2
void arrayPlus(unsigned char *res,unsigned char *a1,unsigned char *a2);
//int= a1[0]*a2[0]+a1[1]*a2[1]----
int arrayMultiply(unsigned char *a1,unsigned char *a2);
class Action
{
    public:
        Action(ACTION_FUN f,int d=100);
        virtual ~Action();
        Action operator +(const Action& a);
        double play(double d=0.01);
        unsigned char used[20];
        unsigned char reject[20];
        void stop(){step=0;run(0);}
        void run(double s);
    static int data[20];
    protected:
        double step;//0~1
        int delay;
        std::vector<ACTION_FUN> fun;
        Action(){};
    private:

};

#endif // ACTION_H
