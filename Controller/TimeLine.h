#ifndef TIMELINE_H
#define TIMELINE_H
#include<vector>
#include <algorithm>


class TIME_POINT
{
    public:
    int start;
    int motor;
    int scale;
    int end;
    TIME_POINT(){}
    TIME_POINT(int a,int b,int c,int d):motor(a),scale(b),start(c),end(d)
    {

    }
    bool operator<(TIME_POINT&b )
    {
        return start<b.start;
    }
};
bool operator <(const TIME_POINT&a,const TIME_POINT& b)
{

    return a.start<b.start;
}
class TimeLine
{
    public:
        int data[20];
        std::vector<TIME_POINT> line;
        int time;//ms
        void add_action(int motor,int scale,int start,int end);
        void add_action(const TIME_POINT& a);
        void play();
        TimeLine();
        virtual ~TimeLine();
    protected:
    private:
};

#endif // TIMELINE_H
