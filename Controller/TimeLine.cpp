#include "TimeLine.h"
#include <unistd.h>
#include<stdio.h>
#include<sys/time.h>
#include "transitHub.h"

TimeLine::TimeLine()
{
    //ctor
    time=0;
    for(int i=0;i<20;i++)
    {
        data[i]=0;
    }
}

TimeLine::~TimeLine()
{
    //dtor
}

void TimeLine::add_action(int motor,int scale,int start,int end)
{
    line.push_back(TIME_POINT(motor,scale,start,end));
}
void TimeLine::add_action(const TIME_POINT &a)
{
    line.push_back(a);
}
void TimeLine::play()
{
    transitHub *port=transitHub::getInstance();
    sort(line.begin(),line.end());
    int last=0;
    for(int i=0;i<line.size();i++)
    {
        TIME_POINT* p=&line[i];
        int d=p->start-last;
        last=p->start;
        usleep(1000*d);
        data[p->motor]=p->scale;
        port->doLoopTx(data,data+18);
        printf("t:%d,motor:%d\n",d,p->motor);
    }
}
