#ifndef GAITFILES_H
#define GAITFILES_H

#include <string>
#include <vector>
#include <sys/time.h>
using std::string;
/*
-formate: *.gaits

n1 n2 - -- - n20 delay
n1 n2 - -- - n20 delay

*/
class gaitFiles
{
    public:
        string filename;
        int steps;
        unsigned char used[20];
        unsigned char reject[20];
        std::vector<int *> gaitArray;
        struct timeval time1;

        bool load(string);
        void save();
        void add(int *);
        void run(double,int *);
        gaitFiles(string);
        gaitFiles();
        virtual ~gaitFiles();
    protected:
    private:
};

#endif // GAITFILES_H
