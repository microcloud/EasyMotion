/*
**Author erow
**
*/
#include <iostream>
#include <sys/time.h>
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <fstream>
#include "transitHub.h"
#include "Serial.h"
#include "Action.h"
#include "readcfg.h"
#include "gaitFiles.h"
gaitFiles g1("go.gaits");
#include "TimeLine.h"
#include "model.h"
#include <eigen3/Eigen/Eigen>
#define PI 3.1415926
#define FOR(I,B,E) for(I=B;I<E;I++)
transitHub* a;
using Eigen::Vector3f;
#define X 10
#define HALF_CIRCLE 614.4

void stand(int data[20],double s)
{
    int a1,a2;
    a1=0;a2=-30;
    data[2]=a1+(a2-a1)*s;
    data[6]=-data[2];
    data[9]=data[2];
    data[13]=-data[9];
}
void left_kick(int data[20],double s)
{
    if(s<0.5)
    {
    data[8]=-256+356*s*2;
    data[11]=-256+256*s*2;
    }
    else
    {
        s-=0.5;
    data[8]=100+s*50;
    data[11]=s*50;
    }

}
struct config* cfg;
Action left_a(stand),kick(left_kick);
void SerialTest()
{
     Serial ss("/dev/ttyUSB0",57600);
    char m[200];
    int num=0;
    sleep(3);
    cout<<"start dev"<<endl;

    while(1){
    while(ss.ReadSerial(m,1))
     {
         cout<<m;
        num=0;
     }
     if(num==0)
        ss.WriteSerial("asd\n",4);
     num++;
    //usleep(1050);

    }
}
void balance(int *data)
{
    data[2]=30;
    data[6]=0;
    //right hip

    data[9]=-20;
    data[13]=20;
    a->doLoopTx(data,data+18);
}
void speedup()
{
    model robot;
    int *data;data=robot.getData();
    balance(data);
    Vector3f v;
    v<<0,0,21.5;
    robot.move_right(v);
    robot.move_left(v);
    a->doLoopTx(data,data+18);
    float angle=-M_PI*9/12,length=18.999;
    v<<0,cos(angle)*length,sin(angle)*length;
    robot.move_left(v);
    a->doLoopTx(data,data+18);
    sleep(1);
    //ready to kick
    int d1=data[8],d2=data[11];
    int i=0;
    angle+=M_PI*4/12;
        length+=2.0;
    v<<0,cos(angle)*length,sin(angle)*length;
    robot.move_left(v);
        a->doLoopTx(data,data+18);
    int num=0;
    while(i++<=num)
    {

        v<<0,cos(angle)*length,sin(angle)*length;
        robot.move_left(v);
        a->doLoopTx(data,data+18);
        printf("d_1:%.3f,d_2:%.3f\n",(data[8]-d1)*300.0/1024,(data[11]-d2)*300.0/1024);
        d1=data[8],d2=data[11];
        angle+=M_PI*4/12/num;
        length+=2.0/num;
    }
    exit(1);

}

int main(int param,char** arg){
    //port:1,baud_rate:57600
    int port,way;
    cfg=cfg_load_file("param.cfg");

    a=transitHub::getInstance();
   // cin>>port>>way;
    port=1;way=57600;
    a->ros_simular=cfg_getnum(cfg,"ros_simular");//false in robot
/**/

    while(!a->portOpen(port,way)){
            cout<<"fail!\n";
    cout<<"(port way)//ttyS$port"<<endl;
    return 1;
    }
    speedup();
    model robot;
    int *p;p=robot.getData();
    //cout<<s<<endl;
    cout<<"choose mode: left ,right,forward,side\n";
    char s;
    while(1)
    {
        cin>>s;
        char t=s;
        float va,vb,vc,f;
        Vector3f v;
        switch(t)
        {
        case 'l':
            cin>>va>>vb>>vc;
            v<<va,vb,vc;
            if(vc>0||(v.array().square().sum()>484))
            {
                cout<<"wrong!"<<endl; continue;
            }
            robot.move_left(v);
            break;
        case 'r':
            cin>>va>>vb>>vc;
            v<<va,vb,vc;
            if(vc>0||(v.array().square().sum()>484))
            {
                cout<<"wrong!"<<endl; continue;
            }
            robot.move_right(v);
            break;
        case 'f':
            cin>>f;
            robot.set_forward(f);
            break;
        case 's':
             cin>>f;
            robot.set_side(f);
            break;
        case 'q':
            exit(1);
        }

        cout<<s<<"|"<<robot.getZMP().adjoint()<<endl;

        a->doLoopTx(p,p+18);
        usleep(200*1000);
    }

        //发送数据，让机器人运动
    cout<<"exit\n";
     a->portClose();
    return 0;
}
