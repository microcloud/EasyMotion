#include "gaitStep.h"
#include <fstream>
#include <iostream>

using namespace Eigen;
gaitStep::gaitStep(string n,int len)
{
    //ctor
    length=len;
    name=n;
    /*
    m_data=new int[len*20];
    m_gait=Map<matrix>(m_data,len,20);*/
    m_gait=matrix(len,20);
    m_data=m_gait.data();
}

gaitStep::~gaitStep()
{
    //dtor
}

 bool gaitStep::pushData(int *d)
{
    int i=0;
    if(step>=length)
        return false;
    copyData(m_data+step*20,d,20,1);
    step++;
    return true;
}
bool gaitStep::popData(int *d)
{
    if(step>=length)
        return false;
    int i=0;
    copyData(d,m_data+step*20,20,1);
    step++;
    return true;
}
void gaitStep::getData(int *d)
{
    copyData(d,m_data,20,length);
}
bool gaitStep::loadFile(string f)
{
    step=0;
    std::ifstream file((f).c_str());
    if(!file)
        return false;
    int d;
    int i=0,j=0;
    motorArray tmp;
    while(file>>d)
    {
        tmp[i++]=d;
        if(i>=20)
        {
            j++;
            i=0;
            if(!pushData(tmp))
            {
                std::cout<<f<<"too big \n";
                return false;
            }

        }
    }

    file.close();
    return true;
}
bool gaitStep::saveFile()
{
    std::ofstream file((("./gaits/"+name).c_str()));

    file<<m_gait;
    file.close();

    return true;
}
