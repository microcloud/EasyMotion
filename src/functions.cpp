#include "functions.h"


gaitStep generateGait_line(motorArray a,motorArray b,int length)
{
    static int num=0;
    gaitStep gait("gene_ling"+('a'+num),length);
    for(int i=0;i<length;i++)
    {
        motorArray tmp;
        for(int j=0;j<20;j++)
        {
            tmp[j]=float(b[j]-a[j])/length*i+a[j];
        }
        gait.pushData(tmp);
    }
    return gait;
}
gaitStep generateGait_rate(motorArray a,motorArray b,int length);
