#ifndef ACTION_H_INCLUDED
#define ACTION_H_INCLUDED

#define scale_p_degree 1024/300
//1024 steps
void jump(double step,double l,int  data[20]);
void jump_l(double step,double l,int data[20]);
void jump_kneel(double step,double l,int data[20]);
void balance(int data[20]);
#endif // ACTION_H_INCLUDED
