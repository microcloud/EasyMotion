#ifndef MOTION_DATA_INTERFACE_H
#define MOTION_DATA_INTERFACE_H

#include <vector>
#define MOTORNUM 20
#define SMOOTHMAXERROR 50

#define RENUM 100
#define IMAGE_PLAT_DELAY 6

#define UNKNOW_DOUBLE_VALUE -9999
#define PLAT_SPEED_AUTO UNKNOW_DOUBLE_VALUE
#define MAX_PLAT_V 70
#define MAX_PLAT_H 110

namespace MOTION
{
    extern double sampletime;
    static double gyro_timeval[2] = {0,0};

    const double feetlength = 14.8;
    const double feetwidth = 7.8;
}

//for gyro
//x forward
//y right
//z down

/////////////////
//for press
// x
// |
// |
// |----->y
//-------------
//|f0      f1 |
//|           |
//|     0     |
//|           |
//|f3      f2 |
//-------------

enum gaitTypes
{
	realtime_gait,			//strategy available       0
	test_gait,				//strategy unavailable
	walkfirst,				//no use
	walklast,				//no use
	walkback_first,			//no use
	walkback_last,			//no use                   5
	smallwalk_firststep,	//no use
	smallwalk_laststep,		//no use
	stepfirst,				//strategy unavailable
	steplast,				//strategy unavailable
	leftmovefirststep,		//strategy unavailable     10
	leftmovelaststep,		//strategy unavailable
	rightmovefirststep,		//strategy unavailable
	rightmovelaststep,		//strategy unavailable
	crouchup,				//no use
	crouchdown,				//no use                   15
	//1
	walk,					//strategy available
	leftturn,				//strategy available
	rightturn,				//strategy available
	walkback,				//strategy available
	smallwalk,				//strategy available       20
	smallwalk_1cm,			//strategy available
	smallwalk_3cm,			//strategy available
	smallwalk_5cm,			//strategy available
	step,					//strategy available
	//2
	leftmove,				//strategy available       25
	leftmovebackward,		//strategy available
	leftmoveforward,		//strategy available
	turnleft_around_ball,	//strategy available
	rightmove,				//strategy available
	rightmovebackward,		//strategy available       30
	rightmoveforward,		//strategy available
	turnright_around_ball,	//strategy available
	//3
	crouch,					//strategy available
	//crouchTogoalie,
	//4
	standup,				//strategy available
	liftleft,				//strategy available       35
	liftright,				//strategy available
	liftboth,				//strategy available
	goodbye,				//strategy available
	leftkick,				//strategy available
	rightkick,				//strategy available       40
	leftsidekick,			//strategy available
	rightsidekick,			//strategy available
	rtkick,                 //strategy available
	walkrightkick,          //strategy available
	walkleftkick,           //                         45
	walkright,
	walkleft,
	dwarfleftkick,
	dwarfrightkick,
	kickSideLeft, //                                   50
	leftdownMove,			//strategy available
	rightdownMove,			//strategy available
	getup_for,				//strategy available
	getup_back,				//strategy available
	//5
	//goalieTocrouch,
	goalieLeft,				//strategy available       55
	goalieRight,			//strategy available
	goalieMid,				//strategy available
	goalieLeft_reset,		//strategy unavailable
	goalieRight_reset,		//strategy unavailable
	goalieMid_reset,		//strategy unavailable     60
	smallwalk_closedloop,   // strategy unavailable
	null_gait,
    wenxi_gait
};
enum HandAction
{
	hand_defaultAction,
	hand_expandArm,
	hand_closeArm,
	hand_pause,
	hand_nullAction
};
enum stabilityStatus
{
	stable,
	frontdown,
	backdown,
	leftdown,
	rightdown,
	null_status
};
enum motionPendingState
{
	motion_finished,
	motion_ongoing,
	motion_unknownState
};
//gyro
class gdataDebug
{
public:
	double GYPO[3];
	double ACCL[3];
	long corresCycle;
public:
	gdataDebug():corresCycle(0){
		for(int i=0;i<3;i++)
		{
			ACCL[i] = 0;
			GYPO[i] = 0;
		}
	};
};
//compass
class compassDataDebug
{
public:
    long x;
    long y;
    long z;
    double temperature;
    long corresCycle;
    compassDataDebug():x(0),y(0),z(0),temperature(0),corresCycle(0){};
};
//battery
class batteryDataDebug
{
public:
    float voltage;
    float percentage;
    long corresCycle;
    batteryDataDebug():voltage(0.0),percentage(0.0),corresCycle(0){};
};
//body angle
class angledataDebug
{
public:
	double angleX;
	double angleY;
	double angleZ;
	long corresCycle;
public:
	angledataDebug():angleX(0),angleY(0),angleZ(0),/*lastangleX(0),lastcommandangleX(0),targetangle(0),*/corresCycle(0){};
};
//body offset
class offsetDebug
{
public:
	//double lastOffsetX;
	//double lastOffsetY;
	double offsetX;
	double offsetY;
	long corresCycle;
public:
	offsetDebug():/*lastOffsetX(0),lastOffsetY(0),*/offsetX(0),offsetY(0),corresCycle(0){};
};
//body delta distance
class deltadataDebug
{
public:
	double m_x;
	double m_y;
	double m_angle;
public:
	deltadataDebug():m_x(0),m_y(0),m_angle(0){};
};
//press data
class fdataDebug
{
public:
	double leftpressure[4];
	double rightpressure[4];
	long corresCycle;
public:
	fdataDebug():corresCycle(0){
		for(int i=0;i<4;i++)
		{
			leftpressure[i] = 0;
			rightpressure[i] = 0;
		}
	};
};
//motor initial pos
class initdataDebug
{
public:
	int initial[MOTORNUM];
	long corresCycle;
public:
	initdataDebug():corresCycle(0){
		for(int i=0;i<MOTORNUM;i++)
		{
			initial[i] = 0;
		}
	};
};
//motor temp
class tempdataDebug
{
public:
	int temperature[MOTORNUM];
	long corresCycle;
public:
	tempdataDebug():corresCycle(0){
		for(int i=0;i<MOTORNUM;i++)
		{
			temperature[i] = 0;
		}
	};
};
//motor vol
class voldataDebug
{
public:
	int voltage[MOTORNUM];
	long corresCycle;
public:
	voldataDebug():corresCycle(0){
		for(int i=0;i<MOTORNUM;i++)
		{
			voltage[i] = 0;
		}
	};
};
//motor pos
class posdataDebug
{
public:
	int position[MOTORNUM];
	long corresCycle;
public:
	posdataDebug():corresCycle(0){
		for(int i=0;i<MOTORNUM;i++)
		{
			position[i] = 0;
		}
	};
};
//motor load
class loaddataDebug
{
public:
	int load[MOTORNUM];
	long corresCycle;
public:
	loaddataDebug():corresCycle(0){
		for(int i=0;i<MOTORNUM;i++)
		{
			load[i] = 0;
		}
	};
};


#endif

